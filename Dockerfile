ARG RUBY_VERSION

FROM ruby:${RUBY_VERSION}

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    build-essential \
    patch \
    ruby-dev \
    zlib1g-dev \
    liblzma-dev \
    && apt-get purge -y && apt-get autoremove -y && apt-get autoclean -y \
    && rm -rf /var/lib/apt/lists/* \
    && gem update --system \
    && gem install compass rubygems-update \
    && update_rubygems

ENTRYPOINT ["/bin/bash", "-c"]

CMD ["compass"]
